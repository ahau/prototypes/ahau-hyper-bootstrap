const dht = require('@hyperswarm/dht')

const node = dht({
  ephemeral: true,
  bootstrap: []
})

const PORT = 49737
node.listen(PORT, function () {
  const address = node.address()
  console.log(address)
})
